﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManagerTT : MonoBehaviour
{
    public UnityEvent OnGameStartEvent;

    [Header("Start Panel")] public TextMeshProUGUI levelNumberText;
    public Button tapButton;
    public Animator startPanelAnimator;
    
    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private static UIManagerTT m_Instance;
    
    [Header("Upper panel")] public Animator upperPanelAnimator;
    public TextMeshProUGUI currentLevelText;
    
    
    [Header("Level Complete")] public Animator levelCompleteAnimator;
    public Button collectButton;
    public ParticleSystem levelCompleteParticle;
    public ParticleSystem levelCompleteParticle2;

    public GameObject tutObject;
    public GameObject stickTutObject;


    [Header("Level Complete")] public GameObject levelFailed;
    public Button tryAgainButton;


    public Button backButton;

    public static UIManagerTT Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
    }

    private void Start()
    {
        currentLevelText.text = "Level " +LevelManager.Instance.gameCurrentLevel.ToString();
        levelNumberText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
        ButtonInteraction();
    }

    public void LevelFailed()
    {
        levelFailed.SetActive(true);
        tryAgainButton.onClick.AddListener(delegate
        {
            GameplayTT.Instance.LoadCurrentScene();

        });
    }

    public void LoadLevelNumber()
    {
        currentLevelText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
        levelNumberText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
    }
    

    private void ButtonInteraction()
    {
       tapButton.onClick.AddListener(delegate
       {
           startPanelAnimator.SetTrigger(EXIT);
          // upperPanelAnimator.SetTrigger(ENTRY);
           OnGameStartEvent.Invoke();
       });

        backButton.onClick.AddListener(delegate
        {
            SceneManager.LoadScene("Intro");

        });
    }
    
    public void ShowLevelComplete()
    {
        levelCompleteAnimator.SetTrigger(ENTRY);
        
        levelCompleteParticle.Play();
        levelCompleteParticle2.Play();

        collectButton.onClick.AddListener(delegate
        {
            GameplayTT.Instance.LoadCurrentScene();
        });
    }
    public void ActivateTut()
    {
        tutObject.SetActive(true);
    }
    public void DeactivateTut()
    {
        tutObject.SetActive(false);
    }

    public void ActivateStickTut()
    {
        stickTutObject.SetActive(true);
    }
    public void DeactivateStickTut()
    {
        stickTutObject.SetActive(false);
    }
}
