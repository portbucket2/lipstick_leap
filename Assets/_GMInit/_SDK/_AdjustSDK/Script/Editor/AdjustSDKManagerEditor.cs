﻿using System.Collections.Generic;
using System.IO;
using com.adjust.sdk;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (AdjustSDKManager))]
public class AdjustSDKManagerEditor : Editor {
    
    #region Private Variables

    private AdjustSDKManager m_Reference;
    private Editor m_AdjustEditor;
    private Editor m_AdjustPurchaseEditor;
    private string m_PathForDynamicEnum = "Assets/_AdjustSDK/DynamicEnum/";
    #endregion

    #region Editor

    private void OnEnable () {

        m_Reference = (AdjustSDKManager) target;
    }

    public override void OnInspectorGUI () {

        serializedObject.Update ();

        EditorGUILayout.PropertyField (serializedObject.FindProperty ("appTokenForAndroid"));
        EditorGUILayout.PropertyField (serializedObject.FindProperty ("appTokenForIOS"));

        DrawLineSeperator ();

        EditorGUI.BeginChangeCheck ();
        //EditorGUILayout.PropertyField (serializedObject.FindProperty ("isProductionBuild"));
        if (EditorGUI.EndChangeCheck ()){
            SerializedProperty t_MarkAsChange = serializedObject.FindProperty("updateAdjustConfiguretion");
            t_MarkAsChange.boolValue = true;
        }

        if (m_Reference.updateAdjustConfiguretion) {
            /*
            if (m_Reference.isProductionBuild) {

                m_Reference.logLevel = AdjustLogLevel.Info;
                m_Reference.environment = AdjustEnvironment.Production;
            } else {

                m_Reference.logLevel = AdjustLogLevel.Verbose;
                m_Reference.environment = AdjustEnvironment.Sandbox;
            }
            */
            m_Reference.updateAdjustConfiguretion = false;
        }

        EditorGUILayout.PropertyField (serializedObject.FindProperty ("startManually"));
        if (m_Reference.startManually) {
            if (m_Reference.showDelayStartPanel) {

                EditorGUILayout.BeginHorizontal (); {
                    m_Reference.showDelayStartPanel = EditorGUILayout.Toggle (
                        "SetDelay",
                        m_Reference.showDelayStartPanel

                    );

                    m_Reference.startDelay = EditorGUILayout.Slider (m_Reference.startDelay, 0f, 5f);
                }
                EditorGUILayout.EndHorizontal ();
            } else {

                m_Reference.showDelayStartPanel = EditorGUILayout.Toggle (
                    "SetDelay",
                    m_Reference.showDelayStartPanel

                );
            }
        }

        ShowAdjustEditor ();

        serializedObject.ApplyModifiedProperties ();
    }

    private void ShowImplementationGuide () {

    }

    private void ShowAdjustEditor () {

        DrawLineSeperator ();
        EditorGUI.indentLevel += 1;

            EditorGUILayout.BeginHorizontal (); {
                m_Reference.showAdjustConfiguretion = EditorGUILayout.Foldout (
                    m_Reference.showAdjustConfiguretion,
                    "Configuretion",
                    true);
                if (GUILayout.Button (m_Reference.showAdjustConfiguretionHelpBox ? "HideHelpBox" : "ShowHelpBox")) {

                    m_Reference.showAdjustConfiguretionHelpBox = !m_Reference.showAdjustConfiguretionHelpBox;
                }

            }
            EditorGUILayout.EndHorizontal ();

            if (m_Reference.showAdjustConfiguretionHelpBox) {
                EditorGUILayout.HelpBox (
                    "#All the configuretion for 'Adjust' EventTracking (Including Revenue) are below" +
                    "\n\n#if 'StartManually' is set to 'True', you can assign some delay when initializing 'AdjustSDK'" +
                    "\n\n#It is highly recommand to initialize the 'AdjustSDK' manually (Source -> Rumman Bhai)",
                    MessageType.Info
                );
            }

            if (m_Reference.showAdjustConfiguretion) {

                EditorGUI.indentLevel += 1;

                DrawLineSeperator();

                m_Reference.showAdvanceAdjustConfiguretion = EditorGUILayout.Foldout (
                    m_Reference.showAdvanceAdjustConfiguretion,
                    "Advance Settings",
                    true
                );

                if (m_Reference.showAdvanceAdjustConfiguretion) {

                    EditorGUI.indentLevel += 1;

                    EditorGUILayout.PropertyField (serializedObject.FindProperty ("logLevel"));
                    EditorGUILayout.PropertyField (serializedObject.FindProperty ("environment"));
                    EditorGUILayout.PropertyField (serializedObject.FindProperty ("eventBuffering"));
                    EditorGUILayout.PropertyField (serializedObject.FindProperty ("sendInBackground"));
                    EditorGUILayout.PropertyField (serializedObject.FindProperty ("launchDeferredDeeplink"));

                    EditorGUI.indentLevel -= 1;
                }

                EditorGUI.indentLevel -= 2;

                //-------------------------
                EditorGUI.indentLevel += 2;

                DrawLineSeperator ();
                EditorGUILayout.BeginHorizontal (); {
                    m_Reference.showAdjustPreConfigEventTracker = EditorGUILayout.Foldout (m_Reference.showAdjustPreConfigEventTracker, "PreConfiguredTracker    :   Event", true);

                    if (m_Reference.showAdjustPreConfigEventTracker) {

                        if (m_Reference.updateEnumListForPreConfigEventTracker) {

                            if (GUILayout.Button ("Update (Required)")) {

                                List<string> t_EnumParameter = new List<string> ();
                                int t_NumberOfAssignedEventTracker = m_Reference.listOfAdjustEventTracker.Count;

                                for (int j = 0; j < t_NumberOfAssignedEventTracker; j++) {
                                    t_EnumParameter.Add (m_Reference.listOfAdjustEventTracker[j].uniqueIdentifier);
                                }
                                GenerateEnum ("AdjustEventTrack", t_EnumParameter, m_PathForDynamicEnum);

                                m_Reference.updateEnumListForPreConfigEventTracker = false;
                            }
                        } else {

                            if (GUILayout.Button ("Add (EventTracker)")) {
                                m_Reference.listOfAdjustEventTracker.Add (new AdjustSDKManager.AdjustEventTracker ());
                            }
                        }

                    }

                    if (GUILayout.Button (m_Reference.showAdjustPreConfigEventTrackerHelpBox ? "HideHelpBox" : "ShowHelpBox")) {

                        m_Reference.showAdjustPreConfigEventTrackerHelpBox = !m_Reference.showAdjustPreConfigEventTrackerHelpBox;
                    }
                }
                EditorGUILayout.EndHorizontal ();

                if (m_Reference.showAdjustPreConfigEventTrackerHelpBox) {

                    EditorGUILayout.HelpBox (
                        "(1) Tap on the 'PreConfiguredTracker : Event' to expand its functionality." +
                        "\n(2) Once it is expand, you can simply add new 'Event' by pressing the 'Add (EventTracker)'." +
                        "\n(3) By taping the 'EventTracker (n) -> UniqueIdentifier', you can configure the following event." +
                        "\n(4) 'UniqueIdentifier' (Not Part of AdjustSDK), is a unique key reference to call the following 'Event'." +
                        "\n(5) Make sure you fill the platform specefic 'EventToken' on Android/iOS." +
                        "\n\nIn Order to call the event, there are 2-ways you can achieve it." +
                        "\n(1) Using -> InvokePreConfigEvent (string t_EventUniqueIdentifier) : By passing 'UniqueIdentifier." +
                        "\n(2) Using -> InvokePreConfigEvent (int t_EventIndex) : By passing its 'Index." +
                        "\n\n#Note : If you want call 'AdjustEventTracking' by passing 'EventToken', use TrackAdjustEvent (string t_EventToken) function instead.",
                        MessageType.Info);
                }

                if (m_Reference.showAdjustPreConfigEventTracker) {

                    EditorGUILayout.Space ();
                    EditorGUI.indentLevel += 1;

                    if (m_Reference.listOfAdjustEventTracker == null)
                        m_Reference.listOfAdjustEventTracker = new List<AdjustSDKManager.AdjustEventTracker> ();

                    int t_NumberOfAssignedEventTracker = m_Reference.listOfAdjustEventTracker.Count;

                    for (int i = 0; i < t_NumberOfAssignedEventTracker; i++) {

                        SerializedProperty t_IsTrackerShowOnEditor = serializedObject.FindProperty ("listOfAdjustEventTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("showOnEditor");
                        SerializedProperty t_UniqueIdentifier = serializedObject.FindProperty ("listOfAdjustEventTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("uniqueIdentifier");

                        EditorGUILayout.BeginHorizontal (); {
                            t_IsTrackerShowOnEditor.boolValue = EditorGUILayout.Foldout (t_IsTrackerShowOnEditor.boolValue, "EventTracker (" + i + ") -> " + t_UniqueIdentifier.stringValue, true);
                            if (GUILayout.Button ("Remove")) {
                                m_Reference.listOfAdjustEventTracker.RemoveAt (i);
                                break;
                            }
                        }
                        EditorGUILayout.EndHorizontal ();

                        if (t_IsTrackerShowOnEditor.boolValue) {

                            EditorGUI.indentLevel += 1;

                            EditorGUI.BeginChangeCheck ();
                            t_UniqueIdentifier.stringValue = EditorGUILayout.TextField ("UniqueIdentifier", t_UniqueIdentifier.stringValue);
                            if (EditorGUI.EndChangeCheck ()) {

                                SerializedProperty t_MarkChange = serializedObject.FindProperty ("updateEnumListForPreConfigEventTracker");
                                t_MarkChange.boolValue = true;
                            }

                            EditorGUILayout.Space ();

                            SerializedProperty t_EventTokenForAndroid = serializedObject.FindProperty ("listOfAdjustEventTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("eventTokenForAndroid");
                            t_EventTokenForAndroid.stringValue = EditorGUILayout.TextField ("Android", t_EventTokenForAndroid.stringValue);

                            SerializedProperty t_EventTokenForIOS = serializedObject.FindProperty ("listOfAdjustEventTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("eventTokenForIOS");
                            t_EventTokenForIOS.stringValue = EditorGUILayout.TextField ("iOS", t_EventTokenForIOS.stringValue);

                            EditorGUI.indentLevel -= 1;
                        }

                    }

                    EditorGUI.indentLevel -= 1;

                }

                
                EditorGUI.indentLevel -= 2;
            }
    }

    private void ShowAdjustPurchaseEditor () {

    }

    #endregion

    #region Configuretion

    private void DrawLineSeperator (float t_Height = 1, Color t_LineColor = new Color ()) {

        if (t_LineColor.a == 0)
            t_LineColor.a = 1f;

        GUIStyle t_GUIStyleForHorizontalLine;
        t_GUIStyleForHorizontalLine = new GUIStyle ();
        t_GUIStyleForHorizontalLine.normal.background = EditorGUIUtility.whiteTexture;
        t_GUIStyleForHorizontalLine.margin = new RectOffset (0, 0, 4, 4);
        t_GUIStyleForHorizontalLine.fixedHeight = t_Height;

        Color t_GUIColor = GUI.color;

        GUI.color = t_LineColor;
        GUILayout.Box (GUIContent.none, t_GUIStyleForHorizontalLine);
        GUI.color = t_GUIColor;
    }

    public void GenerateEnum (string t_EnumName, List<string> t_EnumParameters, string t_Path = "Assets/") {

        string[] t_FoldersName = t_Path.Split ('/');
        string t_PreviousPath = t_FoldersName[0];
        string t_ProgressivePath = t_FoldersName[0];
        int t_NumberOfFolder = t_FoldersName.Length;

        for (int j = 0; j < t_NumberOfFolder - 1; j++) {

            if (!AssetDatabase.IsValidFolder (t_ProgressivePath)) {

                AssetDatabase.CreateFolder (t_PreviousPath, t_FoldersName[j]);
            }

            t_PreviousPath = t_ProgressivePath;
            t_ProgressivePath += "/" + t_FoldersName[j + 1];
        }

        string t_FilePathAndName = t_Path + t_EnumName + ".cs";
        using (StreamWriter streamWriter = new StreamWriter (t_FilePathAndName)) {

            streamWriter.WriteLine ("public enum " + t_EnumName);

            streamWriter.WriteLine ("{");

            int t_NumberOfEnumParameters = t_EnumParameters.Count;
            for (int i = 0; i < t_NumberOfEnumParameters; i++) {

                streamWriter.WriteLine ("\t" + t_EnumParameters[i] + ((i + 1) < t_NumberOfEnumParameters ? "," : ""));
            }

            streamWriter.WriteLine ("}");
        }
        AssetDatabase.Refresh ();
    }

    #endregion
}