using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using GameAnalyticsSDK.Events;
using UnityEngine;

public class GAnalyticsGM : MonoBehaviour,IGameAnalyticsATTListener
{
    public static GAnalyticsGM Instance;

   
    void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            GameAnalytics.RequestTrackingAuthorization(this);
        }
        else
        {
            GameAnalytics.Initialize();
        }
    }

    public void GameAnalyticsATTListenerNotDetermined()
    {
        GameAnalytics.Initialize();
    }
    public void GameAnalyticsATTListenerRestricted()
    {
        GameAnalytics.Initialize();
    }
    public void GameAnalyticsATTListenerDenied()
    {
        GameAnalytics.Initialize();
    }
    public void GameAnalyticsATTListenerAuthorized()
    {
        GameAnalytics.Initialize();
    }


    public void LevelStarted(int levelNumber)
    {
        // Using 1 progression part (01) and score
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level_Start", levelNumber.ToString());
    }
    public void LevelCompleted(int levelNumber)
    {
        // Using 1 progression part (01) and score
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level_Complete", levelNumber.ToString());
    }
    public void LevelFailed(int levelNumber)
    {
        // Using 1 progression part (01) and score
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Level_Failed", levelNumber.ToString());
    }
}
