using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharManager : MonoBehaviour
{
    public List<NPCCharTT> charList;

    public static CharManager Instance;

    public int counter;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        charList[counter].PlayMouthOpen();
    }

    public void CurrentSeletectedChar()
    {
        counter++;
        Debug.Log(counter);
        if (counter != charList.Count)
        {
           // charList[counter].PlayMouthOpen();
        }

        if(counter == 9)
        {
            GameplayTT.Instance.LevelComplete();
        }
    }

    public void PlayLipstickAll()
    {
        for (int i = 0; i < charList.Count; i++)
        {
            charList[i].PlayLipstick();
        }
    }
}
