using System;
using System.Collections;
using System.Collections.Generic;
using PaintIn3D;
using PathCreation;
using PathCreation.Examples;
using UnityEngine;

public class LipstickControllerTT : MonoBehaviour
{
    public static LipstickControllerTT Instance;

    [Range(0f, 50f)] public float moveSpeed;

    public P3dPaintSphere paintTool;

    public Material lipstickMAT;

    public LayerMask layerMask;

    public PathFollow pathFollow;

    private float holdTimer = 0f;

    public Vector3 charOffset;

    public Vector3 lipColorValue;

    [Header("Obstackle")]
    public GameObject colorLipPart;
    public Transform lipstickColorPart;
    public int obstackleCounter;


    [Header("Changing Lip")]
    public Transform lipHolder;
    public bool isActive;
    public int id;
    public Animator animator;
    private int IDLE = Animator.StringToHash("idle");
    private int ROUND = Animator.StringToHash("round");
    private int JUMP = Animator.StringToHash("jump");
    private int JUMP2 = Animator.StringToHash("jump2");
    private int CHOKE = Animator.StringToHash("chok");

    public bool isDown;
    public bool isGameOver;
    public bool isTouchedLip;

    public bool isLipstickMoving;
    public float timer;
    public bool isDoubleTap;
    public bool isObstackle;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
       
    }

    private void Update()
    {
        if (isDown)
        {
            Ray ray = new Ray(transform.position+transform.TransformDirection(Vector3.up)*5, transform.TransformDirection(Vector3.down));

            RaycastHit hit;
           
            if (Physics.Raycast(ray , out hit, 1000f))
            {
                Debug.DrawRay(ray.origin,hit.point - ray.origin, Color.yellow);

               // Debug.Log(hit.transform.name);

                if (hit.transform.CompareTag("NPC"))
                {
                    isObstackle = false;
                    charOffset = new Vector3(0, 0.1f, 0f);
                    paintTool.Scale = new Vector3(.7f, .7f, .7f);
                }
                else if (hit.transform.CompareTag("LIPS"))
                {
                    isObstackle = false;
                    isTouchedLip = true;
                    charOffset = new Vector3(0, 0.08f, 0f);
                    paintTool.Scale = lipColorValue;// new Vector3(1.8f, 1.8f, 2.4f);
                    transform.position = hit.point + charOffset;
                    hit.transform.root.GetComponent<NPCCharTT>().isColored = true;

                    if (hit.transform.root.GetComponent<NPCCharTT>().isMouthOpen && !hit.transform.root.GetComponent<NPCCharTT>().isDied)
                    {
                        Debug.LogError("DIED");
                        hit.transform.root.GetComponent<NPCCharTT>().PlayChoking();
                        PlayChok();
                        hit.transform.root.GetComponent<NPCCharTT>().isDied = true;
                        isGameOver = true;

                        transform.SetParent(hit.transform.root.GetComponent<NPCCharTT>().mouthTrans);
                        transform.localPosition = new Vector3(-0.364f, 0.085f, 0.043f);
                        isDown = false;

                        GameplayTT.Instance.LevelFailed();
                    }
                }
                else if (hit.transform.CompareTag("OBSTACKLE"))
                {
                    isObstackle = true;
                    timer += 0.005f ;
                    if (!isDoubleTap)
                    {
                        if (timer > 0.12f)
                        {
                            transform.position = hit.point;
                        }
                    }
                }
            }
            else
            {
                Debug.DrawRay(ray.origin, transform.TransformDirection(Vector3.down) * 1000, Color.white);
            }
        }
    }

    public void ResetThings()
    {
        isLipstickMoving = true;
        isTouchedLip = false;
        isObstackle = false;
        isDoubleTap = false;
        timer = 0;
       
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("OBSTACKLE"))
        {
            GameObject instant = Instantiate(colorLipPart);
            instant.transform.position = collision.contacts[0].point;
            instant.GetComponent<Rigidbody>().AddForce((Vector3.up + Vector3.left) * 150f);
            Destroy(instant, 5f);

            lipstickColorPart.localScale -= new Vector3(0, 0.33f, 0);

            obstackleCounter++;

            Debug.Log("OBSTACKLE COUNTER HIT GAME OBER" + obstackleCounter);

            GameplayTT.Instance.LevelFailed();
        }

        if (collision.transform.CompareTag("LIPSTICK"))
        {
            if (id == 0)
            {
                Debug.Log("Lipstick Collided HERE  UNDER IF ACTIVE " + collision.transform.name);
               // GroundMove.Instance.CollideLip(collision.transform.GetComponent<LipstickController>());

                Vector3 tempPos = collision.transform.position;
                collision.transform.SetParent(lipHolder);
                collision.transform.localPosition =  new Vector3(-3.595f, -5f, 8.83f);
                collision.transform.localRotation = Quaternion.Euler(0, 0, 0);
               
                Gameplay.Instance.lipstickController = collision.transform.GetComponent<LipstickController>();
                InputController.Instance.lipstickController = collision.transform.GetComponent<LipstickController>();
                collision.transform.GetChild(1).gameObject.SetActive(true);

                transform.parent = null;
                animator.enabled = true;
            }
        }
        if (collision.transform.CompareTag("LIPS"))
        {
            collision.transform.root.GetComponent<NPCCharTT>().PlayKiss();
        }
        if (collision.transform.CompareTag("NPC"))
        {
            //collision.transform.root.GetComponent<NPCCharTT>().PlayAngry();
        }
    }


    private void OnCollisionExit(Collision collision)
    {

        if (collision.transform.CompareTag("LIPSTICK"))
        {
            if (id ==0)
            {
                enabled = false;
                isActive = false;
            }
            else
            {
               Debug.Log("Lipstick Collided HERE   " + collision.transform.name);
               isActive = true;
               enabled = true;
               transform.localRotation = Quaternion.Euler(0, 0, 0);
            }
        }
        
    }

    IEnumerator MoveDownRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.1f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = 7.3f;
        float t_DestValue = 5.05f;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression),transform.position.z);

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(MoveDownRoutine());
    }
    IEnumerator MoveUpRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.15f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = 5.05f;
        float t_DestValue = 8.2f;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), transform.position.z);

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(MoveUpRoutine());
    }

    public void TapDown()
    {
        if (GroundMoveTT.Instance.isReadyToTakeInput)
        {
            isTouchedLip = false;
            isObstackle = false;
            isDoubleTap = false;
            timer = 0;
            isDown = true;
            PlayIDLE();
        }
    }

    public void TapUp()
    {
        if (GroundMoveTT.Instance.isReadyToTakeInput)
        {
            if (isObstackle)
            {
                isDoubleTap = true;
            }
            PlayJUMP();
            GroundMoveTT.Instance.MoveSingleStep();
        }
    }

    public void PlayChok()
    {
        animator.SetTrigger(CHOKE);
    }

    public void PlayIDLE()
    {
        animator.SetTrigger(IDLE);
    }
    public void PlayROUND()
    {
        if (!isGameOver)
        {
            if (!isObstackle)
            {
                animator.SetTrigger(ROUND);
            }
            CharManager.Instance.CurrentSeletectedChar();
        }
    }
    public void PlayJUMP()
    {
        animator.SetTrigger(JUMP);
        //int randValue = UnityEngine.Random.Range(0, 100);
        //if (randValue < 50)
        //{
        //    animator.SetTrigger(JUMP);
        //}
        //else
        //{
        //    animator.SetTrigger(JUMP2);
        //}
    }
}
