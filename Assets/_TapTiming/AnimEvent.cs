using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimEvent : MonoBehaviour
{
    public UnityEvent OnAnimEventMouthOpen;
    public UnityEvent OnAnimEventMouthClose;

    public void MouthOpenTrue()
    {
        OnAnimEventMouthOpen.Invoke();
    }
    public void MouthOpenFalse()
    {
        OnAnimEventMouthClose.Invoke();
    }
}
