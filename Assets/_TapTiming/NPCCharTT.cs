using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCharTT : MonoBehaviour
{
    public int id;
    public Animator animator;


    public SkinnedMeshRenderer bodyMeshRenderer;
    public MeshCollider bodyMeshCollider;

    public SkinnedMeshRenderer upLipMeshRend;
    public MeshCollider upLipMeshCollider;

    public SkinnedMeshRenderer lowLipMeshRend;
    public MeshCollider lowLipMeshCollider;


    public ParticleSystem loveParticle;


    public bool isPlayedAngry;
    public bool isPlayedLips;
    public bool isMouthOpen;
    public bool isDied;

    public bool isColored;

    private int EYE_BLINK = Animator.StringToHash("eyeblink");
    private int EYE_MOVEMENT = Animator.StringToHash("eyemovement");
    private int PLAY_KISS = Animator.StringToHash("kiss");
    private int PLAY_ANGRY = Animator.StringToHash("angry");
    private int MOUTH_OPEN = Animator.StringToHash("mouthopen");
    private int LIPSTICK = Animator.StringToHash("lipstick");
    private int PLAY_CHOKE = Animator.StringToHash("angry1");


    public Transform mouthTrans;



    private void Start()
    {
        UpdateCollider();

        InvokeRepeating("SelectAnimation", 3, 5);
    }

    private float time = 0;
    private bool isUpdated;

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time >= 1f && !isUpdated)
        {
            isUpdated = true;
            time = 0;
            UpdateCollider();
        }
    }


    public void UpdateCollider()
    {
        Mesh colliderMesh = new Mesh();
        bodyMeshRenderer.BakeMesh(colliderMesh);
        bodyMeshCollider.sharedMesh = null;
        bodyMeshCollider.sharedMesh = colliderMesh;


        Mesh upLip = new Mesh();
        upLipMeshRend.BakeMesh(upLip);
        upLipMeshCollider.sharedMesh = null;
        upLipMeshCollider.sharedMesh = upLip;


        Mesh lowLip = new Mesh();
        lowLipMeshRend.BakeMesh(lowLip);
        lowLipMeshCollider.sharedMesh = null;
        lowLipMeshCollider.sharedMesh = lowLip;

    }


    public void SelectAnimation()
    {
        int randValue = Random.Range(0, 100);

        if (GameplayTT.Instance.isGameRunning)
        {
            if (!LipstickControllerTT.Instance.isLipstickMoving)
            {
                if (!isColored)
                {
                    if (randValue < 45)
                    {
                        PlayMouthOpen();
                    }
                    else if (randValue > 45 && randValue < 70)
                    {
                        PlayEyeBlink();
                    }
                    else if (randValue > 70 && randValue < 90)
                    {
                        PlayLipstick();
                    }
                    else if (randValue > 90 && randValue < 100)
                    {
                        PlayEyeMovement();
                    }
                }
            }
            else
            {
                PlayLipstick();
            }
        }
    }

    

    public void MouthOpenFalse()
    {
        isMouthOpen = false;
    }
    public void MouthOpenTrue()
    {
        isMouthOpen = true;
    }

    public void PlayChoking()
    {
        animator.SetTrigger(PLAY_CHOKE);
    }

    public void PlayMouthOpen()
    {
        animator.SetTrigger(MOUTH_OPEN);
    }
    public void PlayLipstick()
    {
        animator.SetTrigger(LIPSTICK);
    }

    public void PlayEyeBlink()
    {
        animator.SetTrigger(EYE_BLINK);
    }
    public void PlayEyeMovement()
    {
        animator.SetTrigger(EYE_MOVEMENT);
    }
    public void PlayKiss()
    {
        isPlayedLips = true;

        if (!isPlayedAngry)
        {
            animator.SetTrigger(PLAY_KISS);
            loveParticle.Play();
        }
    }
    public void PlayAngry()
    {
        isPlayedAngry = true;

        if (!isPlayedLips)
        {
            animator.SetTrigger(PLAY_ANGRY);
        }
    }
}
