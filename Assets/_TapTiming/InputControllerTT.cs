﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody))]
public class InputControllerTT : MonoBehaviour
{
    // Min length to detect the Swipe
    public float MinSwipeLength = 5f;

    private Vector2 _firstPressPos;
    private Vector2 _secondPressPos;
    private Vector2 _currentSwipe;

    private Vector2 _firstClickPos;
    private Vector2 _secondClickPos;

    public static Swipe SwipeDirection;
    public float ReturnForce = 10f;

    public Transform selectedTrans;
    public Camera mainCamera;

    public LipstickController lipstickController;

    public static InputControllerTT Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }


    private void Update()
    {
        DetectSwipe();
    }

    public void DetectSwipe()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);

            if (t.phase == TouchPhase.Began)
            {
                _firstPressPos = new Vector2(t.position.x, t.position.y);

                Ray t_Ray = mainCamera.ScreenPointToRay(_firstClickPos);

                RaycastHit t_RaycastHit;
                int t_LayerMask = ~LayerMask.GetMask("PuzzleEnd");

                LipstickControllerTT.Instance.TapDown();

                if (Physics.Raycast(t_Ray, out t_RaycastHit, 100, t_LayerMask))
                {
                    if (t_RaycastHit.transform.CompareTag("Cube"))
                    {
                        selectedTrans = t_RaycastHit.transform;
                    }
                    else if (t_RaycastHit.transform.CompareTag("Spehre"))
                    {
                        selectedTrans = t_RaycastHit.transform;
                    }
                }
            }

            if (t.phase == TouchPhase.Ended)
            {
                _secondPressPos = new Vector2(t.position.x, t.position.y);
                _currentSwipe = new Vector3(_secondPressPos.x - _firstPressPos.x, _secondPressPos.y - _firstPressPos.y);

                // Make sure it was a legit swipe, not a tap
                if (_currentSwipe.magnitude < MinSwipeLength)
                {
                    SwipeDirection = Swipe.None;

                    LipstickControllerTT.Instance.TapUp();

                    return;
                }

                _currentSwipe.Normalize();

                // Swipe up
                if (_currentSwipe.y > 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
                {
                    SwipeDirection = Swipe.Up;

                    if (selectedTrans != null)
                    {
                       // PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
                    }

                    selectedTrans = null;
                }
                // Swipe down
                else if (_currentSwipe.y < 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
                {
                    SwipeDirection = Swipe.Down;
                    if (selectedTrans != null)
                    {
                        //PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
                    }

                    selectedTrans = null;
                }
                // Swipe left
                else if (_currentSwipe.x < 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                {
                    SwipeDirection = Swipe.Left;
                    if (selectedTrans != null)
                    {
                       // PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
                    }

                    selectedTrans = null;
                }
                // Swipe right
                else if (_currentSwipe.x > 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                {
                    SwipeDirection = Swipe.Right;
                    if (selectedTrans != null)
                    {
                        
                    }

                    selectedTrans = null;
                }
                // Swipe up left
                else if (_currentSwipe.y > 0 && _currentSwipe.x < 0)
                {
                    SwipeDirection = Swipe.UpLeft;
                    if (selectedTrans != null)
                    {
                      
                    }

                    selectedTrans = null;
                }
                // Swipe up right
                else if (_currentSwipe.y > 0 && _currentSwipe.x > 0)
                {
                    SwipeDirection = Swipe.UpRight;
                    if (selectedTrans != null)
                    {
                      
                    }

                    selectedTrans = null;
                }
                // Swipe down left
                else if (_currentSwipe.y < 0 && _currentSwipe.x < 0)
                {
                    SwipeDirection = Swipe.DownLeft;
                    if (selectedTrans != null)
                    {
                       
                    }

                    selectedTrans = null;

                    // Swipe down right
                }
                else if (_currentSwipe.y < 0 && _currentSwipe.x > 0)
                {
                    SwipeDirection = Swipe.DownRight;
                    if (selectedTrans != null)
                    {
                      
                    }

                    selectedTrans = null;
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            LipstickControllerTT.Instance.TapDown();
        }

        if (Input.GetMouseButton(0))
        {
            _firstClickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            Ray t_Ray = mainCamera.ScreenPointToRay(_firstClickPos);

            RaycastHit t_RaycastHit;
            int t_LayerMask = ~LayerMask.GetMask("PuzzleEnd");  
        }
        else
        {
            SwipeDirection = Swipe.None;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _secondClickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            _currentSwipe = new Vector3(_secondClickPos.x - _firstClickPos.x, _secondClickPos.y - _firstClickPos.y);

            // Make sure it was a legit swipe, not a tap
            if (_currentSwipe.magnitude < MinSwipeLength)
            {
                SwipeDirection = Swipe.None;
                Debug.LogWarning("TAP DETECTED!!!");

                LipstickControllerTT.Instance.TapUp();

                return;
            }

            _currentSwipe.Normalize();

            //Swipe directional check
            // Swipe up
            if (_currentSwipe.y > 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
            {
                SwipeDirection = Swipe.Up;
                Debug.Log("Up");
                
               // PlayerStickBlock.Instance.MoveDirection(SwipeDirection);
                if (selectedTrans != null)
                {

                }

                selectedTrans = null;
            }
            // Swipe down
            else if (_currentSwipe.y < 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
            {
                SwipeDirection = Swipe.Down;
                Debug.Log("Down");
                 
                //PlayerStickBlock.Instance.MoveDirection(SwipeDirection);
                if (selectedTrans != null)
                {

                }

                selectedTrans = null;

            }
            // Swipe left
            else if (_currentSwipe.x < 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
            {
                SwipeDirection = Swipe.Left;
                Debug.Log("Left");

                
                //PlayerStickBlock.Instance.MoveDirection(SwipeDirection);
                if (selectedTrans != null)
                {

                }

                selectedTrans = null;
            }
            // Swipe right
            else if (_currentSwipe.x > 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
            {
                SwipeDirection = Swipe.Right;
                Debug.Log("right");
               
               // PlayerStickBlock.Instance.MoveDirection(SwipeDirection);
                if (selectedTrans != null)
                {

                }

                selectedTrans = null;

            } // Swipe up left
            else if (_currentSwipe.y > 0 && _currentSwipe.x < 0)
            {
                SwipeDirection = Swipe.UpLeft;
                Debug.Log("UpLeft");
                if (selectedTrans != null)
                {

                }

                selectedTrans = null;
            }
            // Swipe up right
            else if (_currentSwipe.y > 0 && _currentSwipe.x > 0)
            {
                SwipeDirection = Swipe.UpRight;
                Debug.Log("UpRight");
                if (selectedTrans != null)
                {

                }

                selectedTrans = null;

            }
            // Swipe down left
            else if (_currentSwipe.y < 0 && _currentSwipe.x < 0)
            {
                SwipeDirection = Swipe.DownLeft;
                Debug.Log("DownLeft");
                if (selectedTrans != null)
                {

                }

                selectedTrans = null;
                // Swipe down right
            }
            else if (_currentSwipe.y < 0 && _currentSwipe.x > 0)
            {
                SwipeDirection = Swipe.DownRight;
                Debug.Log("DownRight");
                if (selectedTrans != null)
                {

                }

                selectedTrans = null;
            }
        }
    }
}
