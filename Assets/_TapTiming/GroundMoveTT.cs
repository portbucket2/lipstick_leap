using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMoveTT : MonoBehaviour
{
    public static GroundMoveTT Instance;

    public bool isMoveGround;
    [Range(0f, 10f)] public float moveSpeed;

    [Range(0f, 1)] public float acceleration;
    [Range(0f, 10f)] public float maxSpeed;


    [Header("Lipstick Changer")]
    public List<LipstickController> lipsList;

    public bool isReadyToTakeInput;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void StartGroundMove()
    {
        isMoveGround = true;
    }

    private void Update()
    {
        if (isMoveGround)
        {
            transform.position += new Vector3(0, 0, moveSpeed * Time.deltaTime);

            moveSpeed += acceleration * Time.deltaTime;

            if (moveSpeed > maxSpeed)
                moveSpeed = maxSpeed;
        }

        if (transform.position.z > 24f)
        {
            if (isMoveGround)
            {
                isMoveGround = false;
                Gameplay.Instance.LevelComplete();
            }
        }
    }

    public void CollideLip(LipstickController controller)
    {

    }

    
    public void MoveSingleStep()
    {
        StartCoroutine(MoveSideRoutine());
    }

    IEnumerator MoveSideRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.6f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = transform.position.z;
        float t_DestValue = t_CurrentFillValue + 3f;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        isReadyToTakeInput = false;
        LipstickControllerTT.Instance.ResetThings();
        
        CharManager.Instance.PlayLipstickAll();
        yield return new WaitForSeconds(0.1f);
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));

            if (t_Progression >= 1f)
            {
                LipstickControllerTT.Instance.isLipstickMoving = false;
                
                LipstickControllerTT.Instance.PlayROUND();
                isReadyToTakeInput = true;
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(MoveSideRoutine());
    }

    
}

