using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayTT : MonoBehaviour
{
    public static GameplayTT Instance;

    public InputControllerTT inputController;
    public LipstickControllerTT lipstickController;
    public bool isGameRunning;

    private void Awake()
    {
        //Application.targetFrameRate = 60;
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void StartGame()
    {
        StartCoroutine(StartGameRoutine());
    }

    private IEnumerator StartGameRoutine()
    {
        isGameRunning = true;
        yield return new WaitForEndOfFrame();
        inputController.gameObject.SetActive(true);
        lipstickController.gameObject.SetActive(true);
    }

    public void LevelComplete()
    {
        inputController.gameObject.SetActive(false);
        //lipstickController.gameObject.SetActive(false);
        UIManagerTT.Instance.ShowLevelComplete();
    }

    public void LoadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LevelFailed()
    {
        inputController.gameObject.SetActive(false);
        //lipstickController.gameObject.SetActive(false);
        UIManagerTT.Instance.LevelFailed();
    }
}
