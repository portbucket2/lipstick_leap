using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;

    public InputController inputController;
    public LipstickController lipstickController;
    public bool isGameRunning;

    private void Awake()
    {
        //Application.targetFrameRate = 60;
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void StartGame()
    {
        StartCoroutine(StartGameRoutine());
    }

    private IEnumerator StartGameRoutine()
    {
        AnalyticsAssitantGM.ReportLevelStart(LevelManager.Instance.gameCurrentLevel);

        isGameRunning = true;
        yield return new WaitForEndOfFrame();
        GroundMove.Instance.StartGroundMove();
        inputController.gameObject.SetActive(true);
        lipstickController.gameObject.SetActive(true);
    }

    public void LevelComplete()
    {
        AnalyticsAssitantGM.ReportLevelCompleted(LevelManager.Instance.gameCurrentLevel);

        inputController.gameObject.SetActive(false);
        lipstickController.gameObject.SetActive(false);

        LevelManager.Instance.IncreaseGameLevel();

        UIManager.Instance.ShowLevelComplete();
    }

    public void LevelFailed()
    {
        AnalyticsAssitantGM.ReportMatchFailed(LevelManager.Instance.gameCurrentLevel);

        inputController.gameObject.SetActive(false);
        //lipstickController.gameObject.SetActive(false);
        UIManager.Instance.LevelFailed();
    }

    public void LoadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void GotoNextLevel()
    {
        if (LevelManager.Instance.GetCurrentLevel() > 5)
        {
            PlayerPrefs.DeleteKey("Game_Level_Pref");
        }

        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        string loadScene = "Level" + t_CurrentLevel;

        if (SceneManager.GetActiveScene().name != loadScene)
        {
            SceneManager.LoadScene("Level" + t_CurrentLevel);
        }
    }

    public void LoadAgain()
    {
        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        Debug.Log(t_CurrentLevel + " Scene");
        string loadScene = "Level" + t_CurrentLevel;

        SceneManager.LoadScene("Level" + t_CurrentLevel);

    }
}
