using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCharOM : MonoBehaviour
{
    public Animator animator;
    public Transform parentRef;

    public SkinnedMeshRenderer upLipMeshRend;
    public MeshCollider upLipMeshCollider;

    public SkinnedMeshRenderer lowLipMeshRend;
    public MeshCollider lowLipMeshCollider;


    private int PLAY_CHOKE = Animator.StringToHash("angry1");

    private float time = 0;
    private bool isUpdated;


    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time >= 1f && !isUpdated)
        {
            isUpdated = true;
            time = 0;
            UpdateCollider();
        }
    }


    public void PlayChoking()
    {
        animator.SetTrigger(PLAY_CHOKE);
    }

    public void UpdateCollider()
    {
        Mesh upLip = new Mesh();
        upLipMeshRend.BakeMesh(upLip);
        upLipMeshCollider.sharedMesh = null;
        upLipMeshCollider.sharedMesh = upLip;


        Mesh lowLip = new Mesh();
        lowLipMeshRend.BakeMesh(lowLip);
        lowLipMeshCollider.sharedMesh = null;
        lowLipMeshCollider.sharedMesh = lowLip;

    }
}
