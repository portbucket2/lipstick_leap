using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCChar : MonoBehaviour
{
    public Animator animator;


    public SkinnedMeshRenderer bodyMeshRenderer;
    public MeshCollider bodyMeshCollider;

    public SkinnedMeshRenderer upLipMeshRend;
    public MeshCollider upLipMeshCollider;

    public SkinnedMeshRenderer lowLipMeshRend;
    public MeshCollider lowLipMeshCollider;


    public ParticleSystem loveParticle;


    public bool isPlayedAngry;
    public bool isPlayedLips;


    private int EYE_BLINK = Animator.StringToHash("eyeblink");
    private int EYE_MOVEMENT = Animator.StringToHash("eyemovement");
    private int PLAY_KISS = Animator.StringToHash("kiss");
    private int PLAY_ANGRY = Animator.StringToHash("angry");



    private void Start()
    {
        UpdateCollider();

        InvokeRepeating("SelectAnimation", 1, 10);
    }

    private float time = 0;
    private bool isUpdated;

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time >= 1f && !isUpdated)
        {
            isUpdated = true;
            time = 0;
            UpdateCollider();
        }
    }


    public void UpdateCollider()
    {
        Mesh colliderMesh = new Mesh();
        bodyMeshRenderer.BakeMesh(colliderMesh);
        bodyMeshCollider.sharedMesh = null;
        bodyMeshCollider.sharedMesh = colliderMesh;


        Mesh upLip = new Mesh();
        upLipMeshRend.BakeMesh(upLip);
        upLipMeshCollider.sharedMesh = null;
        upLipMeshCollider.sharedMesh = upLip;


        Mesh lowLip = new Mesh();
        lowLipMeshRend.BakeMesh(lowLip);
        lowLipMeshCollider.sharedMesh = null;
        lowLipMeshCollider.sharedMesh = lowLip;

    }


    public void SelectAnimation()
    {
        int randValue = Random.Range(0, 100);
        if (randValue < 50)
        {
            PlayEyeBlink();
        }
        else
        {
            PlayEyeMovement();
        }
    }


    public void PlayEyeBlink()
    {
        animator.SetTrigger(EYE_BLINK);
    }
    public void PlayEyeMovement()
    {
        animator.SetTrigger(EYE_MOVEMENT);
    }
    public void PlayKiss()
    {
        isPlayedLips = true;

        if (!isPlayedAngry)
        {
            animator.SetTrigger(PLAY_KISS);
            loveParticle.Play();
        }
    }
    public void PlayAngry()
    {
        isPlayedAngry = true;

        if (!isPlayedLips)
        {
            animator.SetTrigger(PLAY_ANGRY);
        }
    }
}
