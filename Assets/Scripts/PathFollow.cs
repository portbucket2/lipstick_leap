using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;

public class PathFollow : MonoBehaviour
{
    public PathCreator pathCreator;
    public EndOfPathInstruction endOfPathInstruction;
    public float speed = 5;
    public float distanceTravelled;
    public Transform moveObject;

    public GameObject moveObjectCache;
    


    void Start()
    {
        if (pathCreator != null)
        {
            // Subscribed to the pathUpdated event so that we're notified if the path changes during the game
            pathCreator.pathUpdated += OnPathChanged;
        }
    }

    void Update()
    {
        if (Gameplay.Instance.isGameRunning)
        {
            distanceTravelled += speed * Time.deltaTime;
        }
        if (moveObject != null)
        {
            moveObject.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
            moveObject.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);
        }
    }

    // If the path changes during the game, update the distance travelled so that the follower's position on the new path
    // is as close as possible to its position on the old path
    void OnPathChanged()
    {
        distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.position);
    }

    public void MoveObjectNull()
    {
        moveObject = null;
    }
    public void MoveObjectAssign()
    {
        moveObject = moveObjectCache.transform;
    }
}