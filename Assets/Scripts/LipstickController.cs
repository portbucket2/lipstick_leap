using System.Collections;
using System.Collections.Generic;
using PaintIn3D;
using PathCreation;
using PathCreation.Examples;
using UnityEngine;

public class LipstickController : MonoBehaviour
{
    public static LipstickController Instance;

    [Range(0f, 50f)] public float moveSpeed;

    public P3dPaintSphere paintTool;

    public Material lipstickMAT;

    public LayerMask layerMask;

    public PathFollow pathFollow;

    private float holdTimer = 0f;

    public Vector3 charOffset;

    public Vector3 lipColorValue;

    [Header("Obstackle")]
    public GameObject colorLipPart;
    public Transform lipstickColorPart;
    public int obstackleCounter;


    [Header("Changing Lip")]
    public Transform lipHolder;
    public bool isActive;
    public int id;
    public Animator animator;

    public bool isGameOver;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
       
    }

    private void Update()
    {
       if (isDown)
        {
            Ray ray = new Ray(transform.position+transform.TransformDirection(Vector3.up)*5, transform.TransformDirection(Vector3.down));

            RaycastHit hit;
           
            if (Physics.Raycast(ray , out hit, 1000f))
            {
                Debug.DrawRay(ray.origin,hit.point - ray.origin, Color.yellow);

                // Debug.Log(hit.transform.name);

                if (hit.transform.CompareTag("NPC"))
                {
                    Debug.Log("NPC HIT");
                    charOffset = new Vector3(0, 0.1f, 0f);
                    paintTool.Scale = new Vector3(.7f, .7f, .7f);
                }
                else if (hit.transform.CompareTag("LIPS"))
                {
                    charOffset = new Vector3(0, 0.14f, 0f);
                    paintTool.Scale = lipColorValue;// new Vector3(1.8f, 1.8f, 2.4f);
                }
                else if (hit.transform.CompareTag("DEAD"))
                {
                    Transform parent = hit.transform.root.GetComponent<NPCCharOM>().parentRef;
                    transform.SetParent(parent);
                    transform.localPosition = new Vector3(0.35f, -0.18f, 0.0f);
                    hit.transform.root.GetComponent<NPCCharOM>().PlayChoking();

                    GroundMove.Instance.isMoveGround = false;
                    Gameplay.Instance.LevelFailed();
                    isDown = false;
                    isGameOver = true;
                    animator.SetTrigger("chok");
                    transform.GetChild(1).gameObject.SetActive(false);
                }
                else
                {
                    if (!isGameOver)
                    {
                        charOffset = new Vector3(0, 0.01f, 0f);
                        paintTool.Scale = new Vector3(.7f, .7f, .7f);
                    }
                }

                transform.position = hit.point + charOffset;
            }
            else
            {
                Debug.DrawRay(ray.origin, transform.TransformDirection(Vector3.down) * 1000, Color.white);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("OBSTACKLE"))
        {
            GameObject instant = Instantiate(colorLipPart);
            instant.transform.position = collision.contacts[0].point;
            instant.GetComponent<Rigidbody>().AddForce((Vector3.up + Vector3.left) * 150f);
            Destroy(instant, 5f);

            lipstickColorPart.localScale -= new Vector3(0, 0.33f, 0);

            obstackleCounter++;

            Debug.Log("OBSTACKLE COUNTER " + obstackleCounter);
        }

        if (collision.transform.CompareTag("LIPSTICK"))
        {
            if (id == 0)
            {
                Debug.Log("Lipstick Collided HERE  UNDER IF ACTIVE " + collision.transform.name);
               // GroundMove.Instance.CollideLip(collision.transform.GetComponent<LipstickController>());

                Vector3 tempPos = collision.transform.position;
                collision.transform.SetParent(lipHolder);
                collision.transform.localPosition =  new Vector3(-3.595f, -5f, 8.83f);
                collision.transform.localRotation = Quaternion.Euler(0, 0, 0);
               
                Gameplay.Instance.lipstickController = collision.transform.GetComponent<LipstickController>();
                InputController.Instance.lipstickController = collision.transform.GetComponent<LipstickController>();
                collision.transform.GetChild(1).gameObject.SetActive(true);

                transform.parent = null;
                animator.SetTrigger("fall");
            }
        }
        if (collision.transform.CompareTag("LIPS"))
        {
            collision.transform.root.GetComponent<NPCChar>().PlayKiss();
        }
        if (collision.transform.CompareTag("NPC"))
        {
            collision.transform.root.GetComponent<NPCChar>().PlayAngry();
        }
    }


    private void OnCollisionExit(Collision collision)
    {

        if (collision.transform.CompareTag("LIPSTICK"))
        {
            if (id ==0)
            {
                enabled = false;
                isActive = false;
            }
            else
            {
               
               Debug.Log("Lipstick Collided HERE   " + collision.transform.name);
               isActive = true;
               enabled = true;
               transform.localRotation = Quaternion.Euler(0, 0, 0);
                

            }
        }
        
    }


    public void ReleaseInput()
    {
        StartCoroutine(ReleaseRoutine());
    }

    IEnumerator ReleaseRoutine()
    {
        yield return new WaitForEndOfFrame();
       
        holdTimer = 0f;
        isDown = false;
        StartCoroutine(MoveUpRoutine());
    }


    public bool isDown;

    public void HoldInput()
    {
       
        holdTimer += Time.deltaTime;

        if (holdTimer > 0.05f)
        {
            if (!isDown)
            {
                isDown = true;
               // StartCoroutine(MoveDownRoutine());
            }
        }
    }

    IEnumerator MoveDownRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.1f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = 7.3f;
        float t_DestValue = 5.05f;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression),transform.position.z);

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(MoveDownRoutine());
    }
    IEnumerator MoveUpRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.15f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = 5.05f;
        float t_DestValue = 7.2f;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), transform.position.z);

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(MoveUpRoutine());
    }
}
