using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMove : MonoBehaviour
{
    public static GroundMove Instance;

    public bool isMoveGround;
    [Range(0f, 10f)] public float moveSpeed;

    [Range(0f, 1)] public float acceleration;
    [Range(0f, 10f)] public float maxSpeed;


    public float maxRange;


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void StartGroundMove()
    {
        isMoveGround = true;
    }

    private void Update()
    {
        if (isMoveGround)
        {
            transform.position += new Vector3( 0,0, moveSpeed * Time.deltaTime);

            moveSpeed += acceleration * Time.deltaTime;

            if (moveSpeed > maxSpeed)
                moveSpeed = maxSpeed;
        }

        if (transform.position.z > maxRange)
        {
            if (isMoveGround)
            {
                isMoveGround = false;
                Gameplay.Instance.LevelComplete();
            }
        }
    }



    public void CollideLip(LipstickController controller)
    {

    }
}
