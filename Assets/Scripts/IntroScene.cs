using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroScene : MonoBehaviour
{
    public Button tapHold;
    public Button tapJump;


    private void Start()
    {
        tapHold.onClick.AddListener(delegate
        {
            SceneManager.LoadScene("TapAndHoldNewCam");

        });

        tapJump.onClick.AddListener(delegate
        {

            SceneManager.LoadScene("TapTiming");
        });
    }
}
